<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;


use App\Currency;

use Exception;
use Illuminate\Console\Command;
use DB;



/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class UpdateCurrency extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "currency:update";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "update currency";

	private function sozdat_slag($stroka) 
	{
		$rus=array('А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я','а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',' ');
		$lat=array('A','B','V','G','D','E','E','GH','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','H','C','CH','SH','SCH','Y','Y','Y','E','YU','YA','a','b','v','g','d','e','e','gh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya',' ');
	  
	  	$stroka = str_replace($rus, $lat, $stroka); // перевеодим на английский
	  	$stroka = str_replace('-', '', $stroka); // удаляем все исходные "-"
	  	$slag = preg_replace('/[^A-Za-z0-9-]+/', '-', $stroka); // заменяет все символы и пробелы на "-"
		return $slag;
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = 'http://www.cbr.ru/scripts/XML_daily.asp';
		
		$xml = (array)simplexml_load_file($url);
		
		$array = [];
		foreach($xml['Valute'] as $k => $v) {
		  $array[$k] = (array)$v;
		}
		
		print_r($array);
		
		
		DB::delete('DELETE FROM currency');
		
		foreach($array as $v)
		{
			$v['Value']   = str_replace(',', '.', $v['Value']);
			$v['Nominal'] = str_replace(',', '.', $v['Nominal']);
			
			DB::insert('INSERT INTO `currency`(`cid`, `name`, `english_name`, `alphabetic_code`, `digit_code`, `rate`) 
			values (?, ?, ?, ?, ?, ?)', 
			[
			  $v['@attributes']['ID']
			, $v['Name']
			, $this->sozdat_slag($v['Name'])
			, $v['CharCode']
			, $v['NumCode']
			, str_replace(',', '.', ''.( floatval( $v['Value'] ) / floatval( $v['Nominal']) ))
			]);
		}
    }
}
