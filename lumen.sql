-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 17, 2020 at 07:11 AM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lumen`
--

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `cid` varchar(15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `english_name` varchar(255) NOT NULL,
  `alphabetic_code` varchar(20) NOT NULL,
  `digit_code` varchar(20) NOT NULL,
  `rate` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `cid`, `name`, `english_name`, `alphabetic_code`, `digit_code`, `rate`) VALUES
(273, 'R01010', 'Австралийский доллар', 'Avstraliyskiy-dollar', 'AUD', '036', 52.3273),
(274, 'R01020A', 'Азербайджанский манат', 'Azerbaydghanskiy-manat', 'AZN', '944', 43.0934),
(275, 'R01035', 'Фунт стерлингов Соединенного королевства', 'Funt-sterlingov-Soedinennogo-korolevstva', 'GBP', '826', 95.671),
(276, 'R01060', 'Армянских драмов', 'Armyanskih-dramov', 'AMD', '051', 0.150774),
(277, 'R01090B', 'Белорусский рубль', 'Belorusskiy-rubly', 'BYN', '933', 29.7758),
(278, 'R01100', 'Болгарский лев', 'Bolgarskiy-lev', 'BGN', '975', 44.1964),
(279, 'R01115', 'Бразильский реал', 'Brazilyskiy-real', 'BRL', '986', 13.6385),
(280, 'R01135', 'Венгерских форинтов', 'Vengerskih-forintov', 'HUF', '348', 0.249666),
(281, 'R01200', 'Гонконгских долларов', 'Gonkongskih-dollarov', 'HKD', '344', 9.4467),
(282, 'R01215', 'Датская крона', 'Datskaya-krona', 'DKK', '208', 11.6053),
(283, 'R01235', 'Доллар США', 'Dollar-SSHA', 'USD', '840', 73.2157),
(284, 'R01239', 'Евро', 'Evro', 'EUR', '978', 86.4092),
(285, 'R01270', 'Индийских рупий', 'Indiyskih-rupiy', 'INR', '356', 0.977467),
(286, 'R01335', 'Казахстанских тенге', 'Kazahstanskih-tenge', 'KZT', '398', 0.174414),
(287, 'R01350', 'Канадский доллар', 'Kanadskiy-dollar', 'CAD', '124', 55.2863),
(288, 'R01370', 'Киргизских сомов', 'Kirgizskih-somov', 'KGS', '417', 0.941076),
(289, 'R01375', 'Китайский юань', 'Kitayskiy-yuany', 'CNY', '156', 10.534),
(290, 'R01500', 'Молдавских леев', 'Moldavskih-leev', 'MDL', '498', 4.37109),
(291, 'R01535', 'Норвежских крон', 'Norveghskih-kron', 'NOK', '578', 8.21255),
(292, 'R01565', 'Польский злотый', 'Polyskiy-zlotyy', 'PLN', '985', 19.6732),
(293, 'R01585F', 'Румынский лей', 'Rumynskiy-ley', 'RON', '946', 17.8767),
(294, 'R01589', 'СДР (специальные права заимствования)', 'SDR-specialynye-prava-zaimstvovaniya-', 'XDR', '960', 103.3147),
(295, 'R01625', 'Сингапурский доллар', 'Singapurskiy-dollar', 'SGD', '702', 53.3215),
(296, 'R01670', 'Таджикских сомони', 'Tadghikskih-somoni', 'TJS', '972', 7.09798),
(297, 'R01700J', 'Турецкая лира', 'Tureckaya-lira', 'TRY', '949', 9.93),
(298, 'R01710A', 'Новый туркменский манат', 'Novyy-turkmenskiy-manat', 'TMT', '934', 20.9487),
(299, 'R01717', 'Узбекских сумов', 'Uzbekskih-sumov', 'UZS', '860', 0.00713812),
(300, 'R01720', 'Украинских гривен', 'Ukrainskih-griven', 'UAH', '980', 2.67455),
(301, 'R01760', 'Чешских крон', 'CHeshskih-kron', 'CZK', '203', 3.30754),
(302, 'R01770', 'Шведских крон', 'SHvedskih-kron', 'SEK', '752', 8.40294),
(303, 'R01775', 'Швейцарский франк', 'SHveycarskiy-frank', 'CHF', '756', 80.4038),
(304, 'R01810', 'Южноафриканских рэндов', 'YUghnoafrikanskih-rendov', 'ZAR', '710', 4.19432),
(305, 'R01815', 'Вон Республики Корея', 'Von-Respubliki-Koreya', 'KRW', '410', 0.0617177),
(306, 'R01820', 'Японских иен', 'YAponskih-ien', 'JPY', '392', 0.685701);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=307;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
